# Base API con Slim framework para PHP

## Empezando
Clonar esta base en un directorio y editar también los archivos:
- **C:\Windows\System32\drivers\etc\hosts** con el siguiente código:
    ```
    127.0.0.1  apinombre.local www.api  .local
    ```
- **\xampp\apache\conf\extra\httpd-vhosts.conf** con el siguiente código:
    ```
    <VirtualHost *:80>
        DocumentRoot "C:\dominios\dominio.local\api\public"
        ServerName www.api.local
        ServerAlias api.local
        <Directory "C:\dominios\dominio.local\api\public">
            AllowOverride All
            Order Allow,deny
            Allow from all
            Require all granted
        </Directory>
    </VirtualHost>  
    ```
**Estos códigos deben adaptarse a nuestra estructura de carpetas, y donde pone api, indicar el nombre que le queramos dar a nuestra api, aunque el nombre _api_ sería válido. Esta configuración está diseñada para ejecutar con Xampp en Windows, aunque es adaptable a otros SO.**

## Contacto
Para cualquie duda que pueda surgir contactar en _manuelesperon77@gmail.com_

### Autor: Manuel Esperon
Adaptación de: https://magna.iessanclemente.net/programacion-web/slim-framework
