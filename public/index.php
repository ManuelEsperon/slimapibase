<?php 
    use Slim\Factory\AppFactory;

    require __DIR__ . '/../vendor/autoload.php';
    require __DIR__ . '/../src/basedatos.php';

    // Creamos la app
    $app = AppFactory::create();

    // Hacemos la conexión con la base de datos
    $pdo = Basedatos::getConexion();

    // Cargamos el fichero de rutas
    require __DIR__ . '/../src/rutas/api.php';

    $app->run();
